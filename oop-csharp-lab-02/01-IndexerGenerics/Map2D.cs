﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */
        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;
        private Tuple<TKey1, TKey2> tkey;
        public Map2D()
        {
            this.values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            
            foreach (TKey1 k1 in keys1)
            {
                foreach (TKey2 k2 in keys2)
                {
                    tkey = new Tuple<TKey1, TKey2>(k1, k2);
                    this.values.Add(tkey, generator.Invoke(k1, k2));
                   
                }
            }

            
            
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            throw new NotImplementedException();
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */

            get
            {
                tkey = new Tuple<TKey1, TKey2>(key1, key2);
                return this.values[tkey];
            }

            set
            {
                
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            throw new NotImplementedException();
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            throw new NotImplementedException();
        }

        public int NumberOfElements
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string ToString()
        {
            string s = "[MAPPA] = ";
            foreach(KeyValuePair<Tuple<TKey1,TKey2>,TValue> item in values)
            {
                s += item.ToString() + "\n";
            }
            return s;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
